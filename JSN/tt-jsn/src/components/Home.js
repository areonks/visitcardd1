import React from 'react'
import {Heading} from "./Heading";
import {UserList} from "./userList";

export const Home = () =>{
    return(
        <div>
            <h1> Home</h1>
            <Heading/>
           <UserList/>
        </div>
    )
};