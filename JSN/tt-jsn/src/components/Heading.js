import React from 'react'
import {Link} from 'react-router-dom'
import {

    Container ,
    Navbar ,
    Nav ,
    NavItem ,
    NavbarBrand
} from 'reactstrap';

export const Heading = () => {
    return (
        <Navbar>
            <Container >
                <NavbarBrand href='/'>Heroes</NavbarBrand>
                <Nav>
                    <NavItem>
                        <Link to='/add' className='btn ml-2'> Add User</Link>
                    </NavItem>
                </Nav>
            </Container>
        </Navbar>
    )
};