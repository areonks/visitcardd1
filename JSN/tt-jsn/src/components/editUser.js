import React , {useState , useContext , useEffect} from 'react'
import {Link , useHistory} from 'react-router-dom'
import {GlobalContext} from "../context/GlobalState";
import {
    Form ,
    FormGroup ,
    Label ,
    Input ,
    Button
} from 'reactstrap';

export const EditUser = (props) => {
    const {users , editUser} = useContext(GlobalContext);

    const [selectedUser , setselectedUser] = useState({
        id: '' ,
        name: ''
    });
    const history = useHistory();
    const currentUserId = props.match.params.id;
    useEffect(() => {
        const userid = currentUserId;
        const selectedUser = users.find(user => user.id === userid)
        setselectedUser(selectedUser)
    } , [currentUserId , users])
    const onSubmit = (e) => {
editUser(selectedUser)
        history.push('/')
    }
    const onChange = (e) => {
setselectedUser({...selectedUser, [e.target.name]: e.target.value})
    }

    return (
        <Form onSubmit={onSubmit}>
            <FormGroup>
                <Label>Name</Label>
                <Input type='text' name={'name'} value={selectedUser.name} onChange={onChange}/>
            </FormGroup>
            <Button type='submit'>edit</Button>
            <Link to='/' className='btn btn-danger ml-2'> cancel</Link>
        </Form>
    )
};