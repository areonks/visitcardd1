import React , {useContext} from 'react'
import {GlobalContext} from "../context/GlobalState";
import {Link} from 'react-router-dom'
import {Button , ListGroup , ListGroupItem} from 'reactstrap';

export const UserList = () => {
    const {users , removeUser} = useContext(GlobalContext);
    return (
        <ListGroup>
            {users.map(users => (
                <ListGroupItem className='d-flex' key={users.id} >
                    <strong>{users.name}</strong>
                    <div className='ml-auto'>
                        <Link to={`/edit/${users.id}`} className='btn ml-2'> edit</Link>
                        <Button onClick={() => removeUser(users.id)} color='danger'>Delete</Button>
                    </div>
                </ListGroupItem>
            ))}
        </ListGroup>
    )
};