export default (state , action) => {
    switch (action.type) {
        case 'remove_User':
            return {
                users: state.users.filter(user => {
                    return user.id !== action.payload
                })
            }
        case 'add_User':
            return {
                users: [action.payload , ...state.users]
            }
        case 'edit_User':
            const updateUser = action.payload;

            const updateUsers = state.users.map(user => {
                if (user.id === updateUser.id) {
                    return updateUser
                }
                return user;
            })
            return {
                users: updateUsers
            }
        default:
            return state
    }
};