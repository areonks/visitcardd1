import React , {createContext , useReducer} from 'react';
import AppReducer from './AppReducer';

const initialState = {
    users: []
};
export const GlobalContext = createContext(initialState);

export const GlobalProvider = ({children}) => {
    const [state , dispatch] = useReducer(AppReducer , initialState)

    const removeUser = (id) => {
        dispatch({
            type: "remove_User" ,
            payload: id
        })
    }

    const addUser = (user) => {
        dispatch({
            type: "add_User" ,
            payload: user
        })
    }

    const editUser = (user) => {
        dispatch({
            type: "edit_User" ,
            payload: user
        })
    }
    return (
        <GlobalContext.Provider value={{
            users: state.users ,
            removeUser ,
            addUser ,
            editUser
        }}>
            {children}
        </GlobalContext.Provider>
    )
}