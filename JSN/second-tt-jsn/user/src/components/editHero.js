import React , {useEffect , useState} from 'react'
import {useParams} from 'react-router-dom'
import axios from "axios";

export let EditHero = () => {

    let [TextRow , item] = useState({name: ''});
    let {id} = useParams();
    useEffect(() => {
        axios
            .get("http://localhost:3012/hero/" + id)
            .then(response => item(response.data))

    } , []);
    const [name , setName] = useState()
    const [nikename , setnikeName] = useState()

    const nikenameChange = (event) => {
        setnikeName(event.target.value)
    }
    let changeHeroData = (e) => {
        e.preventDefault()
        let changedHero = {
            id: id,
            name: name ,
            nikename: nikename,


        }
        axios.put("http://localhost:3012/hero/" + id, {
            changedHero
        })
    };

    return (
        <form onSubmit={changeHeroData}>
            <div key={TextRow.id}>
                <h1>Edit Hero</h1>
                <div>
                    <label htmlFor={'name'}>name</label>
                    <input defaultValue={TextRow.name} id={'name'} onChange={(event) => setName(event.target.value)}/>
                </div>
                <div>
                    <label htmlFor={'nikename'}>nikname</label>
                    <input defaultValue={TextRow.nikename} id={'nikename'} onChange={nikenameChange}/>
                </div>
                <div>
                    <label htmlFor={'discription'}>discription</label>
                    <input defaultValue={TextRow.powers_discr}  type={<textarea name="" id="" cols="30" rows="10"></textarea>} name={"discription"}></input>
                </div>
                <div>
                    <img width={50} height={50} src={TextRow.photo} alt={'errormessage'}/>
                </div>
                <button type="submit">change</button>
            </div>
        </form>
    )
}
