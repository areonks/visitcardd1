const express = require("express");
const bodyParser = require("body-parser");
const mysql = require('mysql2')
const connection = mysql.createConnection({
    host: "localhost" ,
    database: "tt-jsn" ,
    user: "root" ,
    password: ''

})

connection.connect(function (err) {
    if (err) {
        return console.error("Ошибка: " + err.message);
    } else {
        console.log("Подключение к серверу MySQL успешно установлено");
    }
});

let app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

var cors = require('cors')
app.use(cors())

app.get("/hero/:id" , function (req , res) {
    const heroId = req.params
    const sql = "SELECT * FROM heroes WHERE id=? LIMIT 1";
    connection.query(sql , heroId.id , function (error , result) {
        const herro = result
        res.send(herro.pop())
        // console.log(herro);
    })


})
app.put("/hero/:id", function (req, res) {
    const updatedHero = req.body
    console.log(updatedHero)
})

app.get('/hero' , function (req , res) {
    const sql = "SELECT * FROM heroes";
    connection.query(sql , function (error , result) {
        const heroArray = result;
        let herroes = []
        for (let i = 0; i < heroArray.length; i++) {
            let hero = {id: heroArray[i].id , name: heroArray[i].name , photo: heroArray[i].avatar_link};
            herroes.push(hero)
        }

        res.send(herroes);
        console.log(herroes)
    })

})


app.post("/hero" , function (req , res) {
    const heroName = req.body
    const sql = "INSERT INTO heroes(name) VALUES(?)";
    connection.query(sql , heroName.name)
    console.log(heroName.name);
})

app.delete("/hero" , function (req , res) {
    const heroId = req.body
    const sql = "DELETE FROM heroes WHERE id=?";
    connection.query(sql , heroId.id)
    console.log(heroId);
})


app.listen(3012 , function () {
    console.log(('api'));
})
