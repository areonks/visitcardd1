$(function () {
    function setColors() {

        let f = 0;
        for (let a = 1; a <= 6; a++) {
            $('#box' + a).removeClass('blue');
            $('#box' + a).removeClass('red');
            let b = Math.round(Math.random());
            if (b === 1 && f < 3) {
                $('#box' + a).addClass('blue');
                f++;
            } else {
                $('#box' + a).addClass('red');
            }
        }

        if (f === 0) {
            let a = Math.floor(Math.random() * 6) + 1;

            $('#box' + a).addClass('blue');
        }
    }

    setColors()
    $('button').click(() => {
        let count = 0;
        const checkedInputs = $('.input-square:checked');
        const blueCount = $('.blue').length;
        $.each(checkedInputs , function (prop , value) {
            let isBlue = $(value).closest('div').find('label').hasClass('blue');
            if (isBlue) {
                count++;
            }
        });

        if (count === blueCount) {
            alert('ok');
            setColors();
        } else {
            alert('bad');
        }
    })

})